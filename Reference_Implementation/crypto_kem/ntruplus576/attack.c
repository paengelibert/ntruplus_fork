#include "kem.h"
#include "poly.h"
#include "params.h"
#include "rng.h"
#include "api.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	unsigned char entropy_input[48];
	FILE *random = fopen("/dev/random", "r");
	fread(entropy_input, 1, 48, random);
	fclose(random);
	randombytes_init(entropy_input, NULL, 256);

	// Generate keys
	unsigned char pk[NTRUPLUS_PUBLICKEYBYTES];
	unsigned char sk[NTRUPLUS_SECRETKEYBYTES];
	crypto_kem_keypair(pk, sk);

	// Encapsulate
	unsigned char ciphertext[NTRUPLUS_CIPHERTEXTBYTES];
	unsigned char shared_secret[NTRUPLUS_SSBYTES];
	crypto_kem_enc(ciphertext, shared_secret, pk);

	// Test decapsulation
	unsigned char shared_secret2[NTRUPLUS_SSBYTES];
	crypto_kem_dec(shared_secret2, ciphertext, sk);

	// Attack
	unsigned char ctprime[NTRUPLUS_CIPHERTEXTBYTES];
	unsigned char ssprime[NTRUPLUS_SSBYTES];
	poly e, c, cprime;

	int wins = 0;
	for(int i = 0; i < NTRUPLUS_N; i ++) {
		// for(int j = 0; j < NTRUPLUS_N; j ++) {
		// 	e.coeffs[j] = 0;
		// }
		// e.coeffs[i] = 2;
		
		poly_frombytes(&c, ciphertext);
		// poly_add(&cprime, &c, &e);
		// printf("%d ", c.coeffs[i]);
		c.coeffs[i] += 2;
		poly_tobytes(ctprime, &c);

		crypto_kem_dec(ssprime, ctprime, sk);

		int same = 1;
		for(int k = 0; k < NTRUPLUS_SSBYTES; k ++) {
			if(shared_secret[k] != ssprime[k]) {
				same = 0;
				break;
			}
		}
		if(same)
			wins ++;
	}
	printf("wins: %d\n", wins);

	for(int i = 0; i < NTRUPLUS_SSBYTES; i ++) {
		printf("%02x", shared_secret[i]);
	}
	printf("\n");
	for(int i = 0; i < NTRUPLUS_SSBYTES; i ++) {
		printf("%02x", shared_secret2[i]);
	}
	printf("\n");
	
	return 0;
}
