# NTRU+ (https://sites.google.com/view/ntruplus/)

This repository contains the official reference implementation of the NTRU+ key encapsulation mechanism, and an optimized implementation for x86 CPUs supporting the AVX2 instruction set. NTRU+ is submitted to 'Korean Post-Quantum Cryptography Competition' (www.kpqc.or.kr).
